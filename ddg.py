#!/usr/bin/env python3
import os
import re
import discord
import requests
from dotenv import load_dotenv
###############################################################################
#                                                                             #
# Author:  Brennen Murphy                                                     #
#                                                                             #
# Date:    19 Aug 2020                                                        #
#                                                                             #
# Version: 0.6                                                                #
#                                                                             #
###############################################################################

SEARCH = 'https://duckduckgo.com/?q='

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
client = discord.Client()

@client.event
async def on_ready():
    print(f'{client.user} has connected to Discord!')

def getUserFromMention(member):
    return member.mention

@client.event
async def on_message(message):
    if message.author == client.user:
        return
    USER_ID = str(message.author.id)

   
    if message.content.startswith('!ddg') or message.content.startswith('!favorite_search_engine') or message.content.split(' ', 1)[0] == '!bang': 
        remove_search_call = message.content.split(' ', 1)[1]
        ddg_search = (SEARCH+remove_search_call)
          
        if ' ' in str(message.content) and not message.content.startswith('!bang'):
            msg = remove_search_call.replace(' ', '+')
            ddg_search = (SEARCH+msg)
        if ' ' in str(message.content) and message.content.startswith('!bang'):
            msg = remove_search_call = message.content.split(' ', 1)[1]
            msg = msg.replace(' ', '%20')
            ddg_search = SEARCH+'%21'+msg
        response = ddg_search
        await message.channel.send(response)

    if re.search(r"duckit!|!duckit", message.content):
        
        if len(message.content.split()) == 2:
            response = f'Hey {message.content.split()[1]}! Guess what, you can just ask me that question instead of asking something like that for others to search for you. Much appreciated... Thanks.'
        
        else:
            response = f'Hey {getUserFromMention(message.author)}! Guess what, you can just ask me that question instead of asking something like that for others to search for you. Much appreciated... Thanks.'
        await message.channel.send(response)
    
    if message.content.startswith('!help'):
        response = 'To search:\n\n`!ddg <question to ask>`\n\nor\n\n`!favorite_search_engine <question to ask>`\n\nTo use bang shortcut search:\n\n`!bang <word or letter> i.e. !bang w (for wikipedia)`\n\nGet link of all bangs!:\n\n`!bang_list`'
        await message.channel.send(response)
        
    if message.content.startswith('!bang_list'):
        await message.channel.send('Link to the list of all available bangs!: https://duckduckgo.com/bang?q=!')
client.run(TOKEN)
